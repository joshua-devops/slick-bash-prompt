# slick-bash-prompt

slick-bash-prompt is a Bash-script that gives your UNIX/GNU Linux prompt a nice and tidy look.

## Prerequisites

1. Bash
2. Git

## Installation

**1. Begin the installation by cloning the repo.**

```shell
$ git clone http://gitlab.com/joshua-devops/slick-bash-prompt.git
```

**2. Move into the directory**

```shell
$ cd slick-bash-prompt
```

**3. Move/Copy the git_prompt script into an application folder.**
```shell
$ cp git_prompt ~/bin/
```
Remember that the application folder needs to be added to your PATH. Like so,

```shell
...
# Local application folder in home directory.
PATH="$PATH:$HOME/bin"
...
```
in your `~/.bashrc` or similar file.

**4. **
