export PS1='\[\e[1;35m\]\W $(git_prompt)\[\e[1;31m\]❯\[\e[1;33m\]❯\[\e[1;32m\]❯\[\e[m\] '

# Add home directory bin folder
export PATH="$PATH:$HOME/bin"

# Add the following for custom git_prompt preference file.
export GIT_PROMPT_CONFIG="~/.git_prompt_config"
